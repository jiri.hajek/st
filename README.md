# st - simple terminal
This is my build of the [suckless simple terminal (st)](https://st.suckless.org/) based on 9654d7d3 (v0.8.4) with a couple of extra features.

## Installation
```
git clone https://gitlab.com/jiri.hajek/st.git
cd st
sudo make install
```

## Patches
* st-alpha-0.8.2
* st-anysize-0.8.1
* st-font2-20190416-ba72400
* st-newterm-0.8.2
* st-scrollback-0.8.4
* st-vertcenter-20180320-6ac8c8a

## Custom keybindings
* `control-shift-k/j` to zoom in/out
* `alt-k/j` **or** mouse wheel to scroll up/down
* `control-shift-enter` to spawn a new terminal with the same working directory

## Colors
Colors are defined in `config.h`.

![colors](./screenshots/colors.png)

## Fonts
This build uses the `Hack Nerd Font Mono` and `JoyPixels` fonts.

## Emoji support
You'll need the `JoyPixels` font to display emojis.

If you have trouble viewing emojis, install [libxft-bgra](https://aur.archlinux.org/packages/libxft-bgra) and/or [lib32-libxft-bgra](https://aur.archlinux.org/packages/lib32-libxft-bgra) from the AUR. You'll most likely have to do this unless [this MR](https://gitlab.freedesktop.org/xorg/lib/libxft/-/merge_requests/1) has already been merged.

![emojis](./screenshots/emojis.png)
